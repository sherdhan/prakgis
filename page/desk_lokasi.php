<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <title></title>
  <link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
  <link href="../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <script src="http://maps.google.com/maps/api/js?key=AIzaSyCoDisfmbUBn-cleAtfgxWopPMprklqRvE"></script>

    <script>
      var marker;
      var im = 'http://www.robotwoods.com/dev/misc/bluecircle.png';
      var start1, start2;
      var end1, end2;

      function initialize(position){

        var infoWindow = new google.maps.InfoWindow;
        var mapOptions = {
          mapTypeId:google.maps.MapTypeId.ROADMAP,
          zoom:15
        };
        var map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
        var bounds = new google.maps.LatLngBounds();
        var directionsService = new google.maps.DirectionsService;
        var directionsDisplay = new google.maps.DirectionsRenderer({
          map: map
        });

        <?php
        $a= $_GET['nama_warung'];
        $kon= mysqli_connect("localhost","root","");
              mysqli_select_db($kon,"prak_gis");
            $query = mysqli_query($kon,"SELECT * FROM data_location where nama_warung='$a'");
            while ($data = mysqli_fetch_array($query))
            {
                $lat = $data['lat'];
                $lon = $data['lon'];
                echo ("addMarker($lat, $lon);");                        
            }
          ?>
          

        
        function addMarker(lat, lng){
          var lokasi = new google.maps.LatLng(lat, lng);
          end1 = lat;
          end2 = lng;
          bounds.extend(lokasi);
          var marker = new google.maps.Marker({
            map:map,
            position:lokasi
          });
          map.fitBounds(bounds);
        }

        if (navigator.geolocation) {
          navigator.geolocation.getCurrentPosition(function(position) {
            start1 = position.coords.latitude;
            start2 = position.coords.longitude;
            var pos = {
              lat: position.coords.latitude,
              lng: position.coords.longitude
            };
            var lokasi = new google.maps.LatLng(pos);
            bounds.extend(lokasi);
            var marker = new google.maps.Marker({
              map:map,
              position:lokasi,
              icon: im
            });
            map.fitBounds(bounds);
            infoWindow.open(map);

            calculateAndDisplayRoute(directionsService, directionsDisplay, start1, start2, end1, end2);
          });
        }

        function calculateAndDisplayRoute(directionsService, directionsDisplay, start1, start2, end1, end2) {
          directionsService.route({
            origin: new google.maps.LatLng(start1, start2),
            destination: new google.maps.LatLng(end1, end2),
            travelMode: google.maps.TravelMode.DRIVING
          }, function(response, status) {
            if (status == google.maps.DirectionsStatus.OK) {
              directionsDisplay.setDirections(response);
            } else {
              window.alert('Directions request failed due to ' + status);
            }
          });
        }
      }
      google.maps.event.addDomListener(window, 'load', initialize);
    </script>
</head>

<body onload="initialize()">
  <nav class="navbar navbar-inverse">
    <div class="container-fluid">
      <div class="navbar-header">
        <a class="navbar-brand" href="">Sifood Malang</a>
      </div>
      <ul class="nav navbar-nav">
        <li><a href="">Home</a></li>
        <li class="active"><a href="">Detail</a></li>
      </ul>
    </div>
  </nav>
  <div class="container">
    <div class="col-md-12">
      <div class="row">
        
        <div class="col-md-12">
              <div class="panel panel-default">
                <div class="panel-heading">Tempat Makan <strong><?php echo $a ?></strong>  </div>
                <div class="panel-body">
                    <div id="map-canvas" style="width: 100%; height: 518px"></div>
                </div>
              </div>
            </div>
            <div class="col-md-8">
              <div class="panel panel-default">
                <div class="panel-heading">Daftar Menu <strong><?php echo $a ?></strong></div>
                <div class="panel-body">
                                   

                        <table id='example1' class='table table-bordered table-striped'>
                        <thead>
                        <tr>
                        <th>No</th>
                        <th>Menu</th>
                        <th>Harga</th>
                        <th>Keterangan</th>
                        </tr>
                        </thead>
                        <tbody>
                           <?php
                      $i = 1;
                      $jml = 0;
                      $pem =0;
                      $sal =0;
                      function rupiah($angka){
  
                      $hasil_rupiah = number_format($angka,0,',','.');
                      return $hasil_rupiah;
                    }
                      $kon= mysqli_connect("localhost","root","");
                              mysqli_select_db($kon,"prak_gis");
                            $query = mysqli_query($kon,"SELECT * FROM data_location,tb_menu  where tb_menu.id_lokasi=data_location.id_lokasi and nama_warung='$a'");
                            while ($data = mysqli_fetch_array($query))
                            {
                    ?>
                        <tr>
                        <td><?php echo $i ?></td>
                        <td><?php echo $data['nama_menu'] ?></td>
                        <td>Rp. <?php echo $data['harga'] ?></td>
                        <td><?php echo $data['keterangan'] ?></td>
                        </tr>

                        <?php $i++; 
                        } ?>
                    </tbody>
                        <tfoot>

                        </tfoot>
                        
                      </table>

                      <table>
                        <tr>
                        <td>
                          <p><em class="fa fa-book"></em> Deskripsi Toko</p>
                        </td>
                      </tr>
                      <tr>
                        <td>
                          <?php 
                            $kon= mysqli_connect("localhost","root","");
                              mysqli_select_db($kon,"prak_gis");
                            $query = mysqli_query($kon,"SELECT * FROM data_location,tb_menu  where tb_menu.id_lokasi=data_location.id_lokasi and nama_warung='$a'");
                            while ($data = mysqli_fetch_array($query))
                            {
                          ?>
                          <p><?php echo $data['desk'] ?></p>

                          <?php
                            }
                          ?>
                        </td>
                      </tr>
                      <tr>
                        <td>
                          <p><b>Owner : </b><?php echo $data['pemilik'] ?></p>
                        </td>
                      </tr>
                      <tr>
                        <td>
                          <p><em class="fa fa-phone"></em><?php echo $data['no_telp'] ?></p>
                        </td>
                      </tr>

                      </table>

                          
                </div>
              </div>
            </div>  
            </div>
    </div>
  </div>
  <footer>
    <!-- <nav class="navbar navbar-default" style="bottom: 0;margin: 0">
      <div class="container"> 
        <center>
          
        <ul class="nav navbar-nav">
          <li><a>Copyright &#9400; 2018 Nemuomah</a></li>        
        </ul>

        <ul class="nav navbar-nav navbar-right">
          <li><a>Software Quality</a></li>                  
        </ul>
        </center>   
      </div>
    </nav> -->
  </footer>
    <script src="../vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
</body>
</html>